class BlueprintMobile {
    constructor(tipe, warna, apakahmobilbalap){
        this.tipe = tipe;
        this.warna = warna;
        this.apakahmobilbalap = apakahmobilbalap;
    }

    hidupinMesin(){
        console.log('Mobil $(this.tipe) warna $(this.warna) dihidupin');
    }
}
//Pemanggilan class dengan menggunakan keyword ‘new’bertujuan untuk membuat instance atau turunan dari kerangka class yang telah dibuat, kemudian dari turunan itu akan dibuat menjadi object
const mobilmobilan = new BlueprintMobile('avansa', 'kuning', false);
console.log(mobilmobilan);