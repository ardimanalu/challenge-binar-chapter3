class BlueprintMobile {
    constructor(tipe, warna, apakahmobilbalap){
        this.tipe = tipe;
        this.warna = warna;
        this.apakahmobilbalap = apakahmobilbalap;
    }

    ubahwarnamobil(newwarna){
        this.warna = newwarna;
        console.log('gantiwarna baru');
    }
}

const mobil = new BlueprintMobile('avansa', 'abuabu', false);
console.log(mobil);

mobil.ubahwarnamobil('merah');
console.log(mobil);