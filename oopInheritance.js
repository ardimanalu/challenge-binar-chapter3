class manusia {
    constructor(nama, alamat){
        this.nama = nama;
        this.alamat = alamat;
    }
    sapa(){
        console.log('Halo, nama saya', this.nama );
    }
    kerja(){
        console.log("lagi Kerja Nih...");
    }
}

class guru extends manusia {
    constructor(namaguru, alamatguru, BidangMataPelajaran){
        super(namaguru, alamatguru);
        this.BidangMataPelajaran = BidangMataPelajaran;
    }
    sapamurid(){
        super.sapa();
    }
    kerjain(){
        super.kerja();
    }
}


const guruku =  new guru(
    "Ardi Manalu",
    "jl. Ryachudu, Bandar Lampung",
    "TIK",
);

guruku.sapamurid();
guruku.kerjain();
console.log(guruku);
