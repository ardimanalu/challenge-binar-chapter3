import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {Splash, HomeScreen, MovieDetail} from '../pages';


const Stack = createNativeStackNavigator();


const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Splash">
    <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }}/>
    <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }}/>
    <Stack.Screen name="MovieDetail" component={MovieDetail} options={{ headerShown: false }}/>   
    </Stack.Navigator>
  );
};

// <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }}/>

export default Router;

const styles = StyleSheet.create({

});
