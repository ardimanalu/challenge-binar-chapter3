import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {moderateScale} from 'react-native-size-matters';
import {Back, Share, Love} from '../../assets';
import {useNavigation} from '@react-navigation/native';
import {AirbnbRating} from 'react-native-ratings';

const DetailPages = props => {
  const navigation = useNavigation();
  return (
    <View style={styles.page}>
      <ImageBackground
        source={{uri: props.itemid.poster_path}}
        style={styles.header}>
        <View style={styles.icontop}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image source={Back} style={styles.headerIcon} />
          </TouchableOpacity>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
              <Image source={Love} style={[styles.headerIcon, styles.love]} />
            </TouchableOpacity>

            <TouchableOpacity>
              <Image source={Share} style={styles.headerIcon} />
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>

      <View style={styles.container}>
        <View>
          <Image
            style={styles.Image}
            source={{uri: props.itemid.poster_path}}
          />
        </View>
        <View style={styles.card}>
          <Text style={styles.judul}>Judul: {props.itemid.title}</Text>
          <Text style={styles.Tanggal}>
            Tanggal Rilis : {props.itemid.release_date}
          </Text>         
          <View>
            <Text style={styles.Tanggal}>
              Durasi: {props.itemid.runtime} Menit
            </Text>
            <AirbnbRating
              ratingContainerStyle={styles.Rating}
              count={5}
              reviews={[
                'Sangat Tidak Bagus',
                'Tidak Bagus',
                'Oke',
                'Bagus',
                'Bagus banget',
              ]}
              reviewSize={14}
              defaultRating={4}
              size={13} 
            />
          </View>
        </View>
      </View>
      <View>
        <Text style={styles.Genre}>Genre</Text>
        <View style={styles.genreContainer}>
          <FlatList
            horizontal={true}
            data={props.itemid.genres}
            keyExtractor={index => index}
            renderItem={({item}) => (
              <Text style={styles.genreText}>{item.name}</Text>
            )}
          />
        </View>
      </View>
      <View>
        <Text style={styles.Genre}>Synopshis</Text>
        <Text style={styles.Synopshis}>{props.itemid.overview}</Text>
      </View>
      <View>
        <Text style={styles.Tanggal}>Actors/Artist</Text>
        <View style={styles.artis}>
          <FlatList
            numColumns={3}
            keyExtractor={(item, index) => index}
            data={props?.itemid?.credits?.cast}
            renderItem={({item}) => (
              <View>
                <Image
                  source={{uri: item.profile_path}}
                  style={styles.Imageartis}
                />
                <Text style={styles.namaartis}>{item.name}</Text>
              </View>
            )}
          />
        </View>
      </View>
    </View>
  );
};

export default DetailPages;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  header: {
    width: windowWidth * 1,
    height: windowHeight * 0.4,
    borderRadius: 10,
  },
  icontop: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 12,
    marginHorizontal: 20,
  },
  headerIcon: {
    height: 30,
    width: 30,
  },
  love: {
    marginRight: 20,
  },
  container: {
    backgroundColor: 'rgba(83, 200, 204, 1)',
    width: windowWidth * 0.92,
    height: windowHeight * 0.23,
    marginHorizontal: 30,
    borderRadius: 10,
    shadowColor: ' rgba(249, 87, 188, 0.5) ',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
    marginTop: -windowHeight * 0.12,
    flexDirection: 'row',
    marginLeft: 16,
    display: 'flex',
  },
  Image: {
    width: windowWidth * 0.3,
    height: windowHeight * 0.2,
    marginLeft: windowWidth * 0.04,
    marginTop: windowHeight * 0.017,
    borderRadius: 5,
  },
  card: {
    marginTop: windowHeight * 0.02,
  },
  judul: {
    width: windowWidth * 0.5,
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginLeft: 20,
    fontSize: windowWidth * 0.045,
    color: 'rgba(254, 83, 187, 1)',
  },
  Tanggal: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 13,
    marginTop: windowHeight * 0.009,
    color: 'rgba(254, 83, 187, 1)',
  },
  Rating: {
    width: windowWidth * 0.4,
  },
  Genre: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 18,
    marginTop: windowHeight * 0.009,
    color: 'rgba(254, 83, 187, 1)',
  },
  Synopshis: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 14,
    marginTop: windowHeight * 0.009,
    color: '#FFF',
    width: windowWidth * 0.94,
  },
  artis: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  Imageartis: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.18,
    marginLeft: windowWidth * 0.05,
    marginRight: windowWidth * 0.026,
    marginTop: 15,
    borderRadius: 10,
  },
  namaartis: {
    width: windowWidth * 0.2,
    alignItems: 'center',
    fontWeight: 'bold',
    marginLeft: windowWidth * 0.06,
    fontSize: 13,
    marginTop: windowHeight * 0.0003,
    color: '#FFF',
  },
  genreSemiContainer: {
    paddingHorizontal: moderateScale(3),
    marginTop: moderateScale(5),
  },
  genreContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: windowWidth * 0.1,
  },
  genreText: {
    fontSize: moderateScale(16),
    color: '#fff',
    paddingLeft: 5,
  },
});
//flex-wrap digunakan untuk mendefinisikan bahwa elemen item di dalam container flexbox tidak harus disejajarkan dalam satu baris. Artinya, elemen item tersebut digulung (dipindahkan) ke baris baru bila sudah memenuhi lebar container-nya.
