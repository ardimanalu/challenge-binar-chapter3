import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import React from 'react';

import {useNavigation} from '@react-navigation/native';

function Recommended(props) {
  const navigation = useNavigation();

  return (
    <View>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={props.data}
        keyExtractor={item => item.id}
        renderItem={({item}) => (
          <TouchableOpacity
            style={styles.posterContainer}
            onPress={() =>
              navigation.navigate('MovieDetail', {movieId: item.id})
            }>
            <Image
              style={styles.Imageposter}
              source={{uri: item.poster_path}}
            />
          </TouchableOpacity>
        )}
      />
    </View>
  );
}

export default Recommended;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  Imageposter: {
    width: windowWidth * 0.4,
    height: windowHeight * 0.3,
    marginLeft: windowWidth * 0.06,
    borderRadius: 20,
  },
});
