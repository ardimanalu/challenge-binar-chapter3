import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import React from 'react';
import {moderateScale} from 'react-native-size-matters';
import {useNavigation} from '@react-navigation/native';

const Genres = {
  //genres didapat dari https://developers.themoviedb.org/3/genres/get-movie-list
  Action: 28,
  Adventure: 12,
  Animation: 16,
  Comedy: 35,
  Crime: 80,
  Documentary: 99,
  Drama: 18,
  Family: 10751,
  Fantasy: 14,
  History: 36,
  Horror: 27,
  Music: 10402,
  Mystery: 9648,
  Romance: 10749,
  SciFi: 878,
  TV: 10770,
  Thriller: 53,
  War: 10752,
  Western: 37,
};
const Genre = props => {
  let keys = props.keys;
  let list = props.list;
   //map  proses looping data sama halnya dengan for each yang berfungsi untuk menapilkan data dari sebuah objact
  //Ketika kamu memanggil map pada suatu array, map akan menjalankan callback tersebut pada setiap elemen di dalamnya, mengembalikan sebuah array baru dengan semua nilai yang dikembalikan oleh callback.
  return keys.map((key, index) => {
   //ngebuat variabel genre dimana object.keys akan mereturn list dengan mencari data genre sesuai dengan keynya atau idnya
    let genre = Object.keys(list).find(data => list[data] === key);
    //mereturn
    return (
      <View style={styles.genreSemiContainer} key={index}>
        <Text style={styles.genreText}>{genre}</Text>
      </View>
    );
  });
};

function Upload(props) {
  const navigation = useNavigation();

  return (
    <FlatList
      data={props.data}
      keyExtractor={item => item.id}
      renderItem={({item}) => (
        <View style={styles.viewList}>
          <View>
            <Image style={styles.Image} source={{uri: item.poster_path}} />
          </View>
          <View style={styles.card}>
            <Text style={styles.judul}>Judul: {item.title}</Text>
            <Text style={styles.Tanggal}>Tanggal: {item.release_date}</Text>
            <Text style={styles.Tanggal}>Rating : {item.vote_average}</Text>
            <View style={styles.genreContainer}>
              {/* <Text style={styles.Tanggal}>Genre : </Text> */}
              <Genre
                style={styles.genreText}
                keys={item.genre_ids}//sesuai id genre
                list={Genres} //list genre
              />
            </View>
            <TouchableOpacity
              style={styles.tombol}
              onPress={() =>
                navigation.navigate('MovieDetail', {movieId: item.id})
              }>
              <View>
                <Text style={styles.textshow}>Show More</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )}
    />
  );
}

export default Upload;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  viewList: {
    display: 'flex',
    borderWidth: 1,
    borderColor: '#27756c',
    flexDirection: 'row',
  },
  Image: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.18,
    marginLeft: 20,
    marginTop: 15,
    marginBottom: 15,
    borderRadius: 10,
  },
  card: {
    marginTop: windowHeight * 0.02,
  },
  judul: {
    width: windowWidth * 0.6,
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginLeft: 20,
    fontSize: 16,
    color: '#fff',
  },
  Tanggal: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 12,
    marginTop: windowHeight * 0.009,
    color: '#fff',
  },
  tombol: {
    backgroundColor: '#25b3a2',
    padding: 5,
    alignItems: 'center',
    width: windowWidth * 0.3,
    marginLeft: windowWidth * 0.05,
    borderRadius: 8,
    marginVertical: windowHeight * 0.015,
  },
  textshow: {
    fontWeight: '700',
    fontSize: 12,
    color: 'white',
  },
  genreSemiContainer: {
    paddingHorizontal: moderateScale(3),
    marginTop: moderateScale(5),
  },
  genreContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: windowWidth * 0.04,
  },
  genreText: {
    fontSize: moderateScale(10),
    color: '#fff',
  },
});

//flex-wrap digunakan untuk mendefinisikan bahwa elemen item di dalam container flexbox tidak harus disejajarkan dalam satu baris. Artinya, elemen item tersebut digulung (dipindahkan) ke baris baru bila sudah memenuhi lebar container-nya.
