import axios from 'axios'
//‘Async’ akan mengubah function menjadi asynchronous, sementara ‘await’ akan menunda eksekusi hingga proses asynchronous selesai.

export const getMovie = async (setData, setScreenStatus) => {

  await axios
    .get(`http://code.aldipee.com/api/v1/movies`)
    .then(req => {
      setData(req.data)
      setScreenStatus(true)
    })
    .catch(err => {
      console.log(err);
    })
}