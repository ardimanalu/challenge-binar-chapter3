import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';
import NetInfo from '@react-native-community/netinfo';
import React, {useEffect, useState} from 'react';

const App = () => {
  const [isOffline, setOfflineStatus] = useState(false);

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener(state => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });

    return () => removeNetInfoSubscription();
  }, []);

  return (
    <NavigationContainer>
      {isOffline ? <Text>Koneksi Gagal, silahkan nyalakan internet terlebih dahulu</Text> : <Router />}
    </NavigationContainer>
  );
};

export default App;
