import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
} from 'react-native';
import React, {useEffect} from 'react';
import {Logo} from '../../assets';

// setTimeout navigation
//replace - replace rute saat ini dengan yang baru
//push  - push a new route onto the stack dorong rute baru ke stack navigasi
//pop - kembali ke stack navigasi sebelumnya

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 5000);
  }, [navigation]);
  return (
    <View style={styles.container}>
      <Image style={styles.Logo} source={Logo} />
      <Text style={styles.Appnama}> Watch movies in Virtual Reality</Text>
      <Text style={styles.nama}> Rangon App </Text>
      <Text style={styles.namadev}> Ardi Gaya Manalu </Text>
    </View>
  );
};

export default Splash;
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#19191B',
    alignItems: 'center',
  },
  Logo: {
    width: windowWidth,
    height: windowHeight * 0.7,
  },
  Appnama: {
    color: '#fff',
    width: windowWidth * 0.8,
    fontSize: 34,
    marginTop: windowHeight * -0.18,
    textAlign: 'center',
  },
  nama: {
    color: 'rgba(255, 255, 255, 0.75)',
    fontSize: 16,
    marginTop: 10,
  },
  namadev: {
    color: 'rgba(255, 255, 255, 0.75)',
    fontSize: 13,
    marginTop: windowHeight * 0.23,
  },
});
