import HomeScreen from "./Home";
import Splash from "./Splash";
import MovieDetail from "./MovieDetail"

export {Splash, HomeScreen, MovieDetail}