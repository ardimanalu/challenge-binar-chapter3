import {
  StyleSheet,
  StatusBar,
  FlatList,
  View,
  Image,
  Text,
  ImageBackground,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useIsFocused} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';
import DetailPages from '../../components/DetailPage/detail';
import {getMovieid} from '../../components/API';
import {Homeback} from '../../assets';

const MovieDetail = ({route}) => {
  const [screenStatus, setScreenStatus] = useState(true);
  const [data, getData] = useState({});

  useEffect(() => {
    getMovieid.getMovieid(getData,setScreenStatus, route.params.movieId);
  }, []);

  function DetailScreenStatusBar() {
    const focus = useIsFocused();

    return focus ? <StatusBar backgroundColor={'#0e1826'} /> : null;
  }  
  if (screenStatus) {
    return (
      <ImageBackground
        source={Homeback}
        resizeMode="cover"
        style={styles.image}>
        <SafeAreaView>
          <FlatList
            keyExtractor={(index) => index}
            ListHeaderComponent={() => (
              <View>
                <DetailScreenStatusBar />
                <DetailPages itemid={data} />
              </View>
            )}
          />
        </SafeAreaView>
      </ImageBackground>
    );
  }
};

export default MovieDetail;

const styles = StyleSheet.create({
  image:{
    flex: 1,
    backgroundColor: '#19191B',
  }   
});
