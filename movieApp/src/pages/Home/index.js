import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Dimensions,
  ImageBackground,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useIsFocused} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {getMovie} from '../../components/API';
import Recommended from '../../components/HomPage/recommended';
import {Homeback} from '../../assets';
import Upload from '../../components/HomPage/upload';

const HomeScreen = () => {
  const [screenStatus, setScreenStatus] = useState(true);
  const [data, getData] = useState({});

  useEffect(() => {
    getMovie.getMovie(getData, setScreenStatus);
  }, []);

  function HomeScreen() {
    const focus = useIsFocused();

    return focus ? <StatusBar backgroundColor={'#0e1826'} /> : null;
  }
  //Tujuan SafeAreaView adalah untuk merender konten dalam batas area sesuai perangkat.
  if (screenStatus) {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground
          source={Homeback}
          resizeMode="cover"
          style={styles.image}>
          <HomeScreen />
          <Text style={styles.recomm}>Recommended</Text>
          <Recommended data={data.results} />
          <Text style={styles.recomm}>Latest Upload</Text>
        <Upload data={data.results} />
        </ImageBackground>
      </SafeAreaView>
    );
  }
};

export default HomeScreen;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#19191B',
  },
  recomm: {
    marginTop: windowHeight * 0.03,
    width: windowWidth,
    height: windowHeight * 0.05,
    color: '#fff',
    marginLeft: windowWidth * 0.06,
    fontSize: 18,
  },
});
