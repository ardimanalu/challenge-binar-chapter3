//Yaitu sebuah property yang dapat diakses setelah si object kamu instantiate (dibuat melalui keyword new). Jadi, kalau ada nilai pada property yang ingin kamu ubah, dia masih bisa diakses. Instance property bisa kamu gunakan untuk nilai/value dari sebuah property yang sifatnya berubah-ubah.


//membuat class
class myclass {
    //menggunakan konsturktor method
    constructor(){
        this.namadepan = "ardi";
        this.namabelakang = "manalu";
    }
}
//pada javascript construktor yang akan dipanggil jika kita mengdelklasrasikan object menggunakan new (new object)
const data = new myclass();


console.log(data);

//contoh penerapatannya
console.log(data.namadepan);
