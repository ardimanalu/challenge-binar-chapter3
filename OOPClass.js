class Employe {
  nama = 'Ardi Manalu';
 
  cetakNama() {
	console.log(this.nama);
  }
}
//Pemanggilan class dengan menggunakan keyword ‘new’bertujuan untuk membuat instance atau turunan dari kerangka class yang telah dibuat, kemudian dari turunan itu akan dibuat menjadi object
const employe1 = new Employe();
employe1.cetakNama()