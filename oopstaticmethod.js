class BlueprintMobile {
    constructor(tipe, warna, apakahmobilbalap){
        this.tipe = tipe;
        this.warna = warna;
        this.apakahmobilbalap = apakahmobilbalap;
    }

    ubahwarnamobil(newwarna){
        this.warna = newwarna;
        console.log('ganti warna baru');
    }
    static hidupkanmobil(){
        console.log("mobil dihidupkan");
    }
}

const mobil = new BlueprintMobile('avansa', 'abuabu', false);
console.log(mobil);

mobil.ubahwarnamobil('merah');
console.log(mobil);

BlueprintMobile.hidupkanmobil();
