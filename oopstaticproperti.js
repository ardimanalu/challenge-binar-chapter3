//Yaitu sebuah property yang nilainya akan selalu sama di semua instance dari class tersebut. Tipe property ini bisa kamu gunakan untuk nilai/value dari sebuah property yang sifatnya tetap.

//membuat class
class myclass {
    //membuat static properti
    static mystaticproperty = "hello"
}
//akses static properti mystaticproperty pada clas myclass
console.log(myclass.mystaticproperty);